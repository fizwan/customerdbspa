from flask import Flask, jsonify, request
import uuid
from flask_cors import CORS


# configuration
DEBUG = True
CUSTOMERS = [
    {
        'id': uuid.uuid4().hex,
        'name': 'John',
        'age': '23',
        'member': True
    },
    {
        'id': uuid.uuid4().hex,
        'name': 'Smith',
        'age': '40',
        'member': False
    },
    {
        'id': uuid.uuid4().hex,
        'name': 'Adam',
        'age': '30',
        'member': True
    }
]

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

@app.route('/customers', methods=['GET','POST'])
def all_customers():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        CUSTOMERS.append({
            'id': uuid.uuid4().hex,
            'name': post_data.get('name'),
            'age': post_data.get('age'),
            'member': post_data.get('member'),
        })
        response_object['message'] = 'Customer Details Added!'
    else:
        response_object['customers'] = CUSTOMERS
    return jsonify(response_object)

@app.route('/customers/<customer_id>', methods=['PUT', 'DELETE'])
def one_customer(customer_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        print(post_data)
        remove_customer(customer_id)
        CUSTOMERS.append({
            'id': uuid.uuid4().hex,
            'name': post_data.get('name'),
            'age': post_data.get('age'),
            'member': post_data.get('member')
        })
        response_object['message'] = 'Customer updated!'
    if request.method == 'DELETE':
        remove_customer(customer_id)
        response_object['message'] = 'Customer removed!'
    return jsonify(response_object)

def remove_customer(customer_id):
    for customer in CUSTOMERS:
        if customer['id'] == customer_id:
            CUSTOMERS.remove(customer)
            return True
    return False

if __name__ == '__main__':
    app.run()
